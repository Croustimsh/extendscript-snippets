/*var proj = app.project;
var comp= proj.selection[0];
var om_name= "_HAP";
var path= "C:/temp";
var NewName = "NewName";
sendCompToRender(comp,om_name,path,NewName)
*/
//declaration:
// sendCompToRender(comp,om_name,path);
///function declaration

function sendCompToRender(comp,om_template,path,customName){
	
	var curRQItem = app.project.renderQueue.items.add(comp);

	// This is the new way to change the output file path of output module 1 of
	// render queue item 1.
	// In this example, the output file is routed to the user directory,
	// but this time using the full path.

	var om1 = curRQItem.outputModule(1);

	if (!customName){
		var file_name = File.decode( om1.file.name ); // Name contains special character, such as space?
	}
	else{


		var file_name = "/"+customName;
		
		}


	var new_path = path.toString();
	var separator = "/";
	if ($.os.indexOf("Mac") == -1)
	{
	new_path = path;
	separator = "\\";
	}
		var new_data = {
		"Output File Info":
		{
		"Full Flat Path":new_path + separator + file_name,


		}
		};



	om1.setSettings( new_data );
	curRQItem.applyTemplate("Best Settings");
	om1.applyTemplate(om_template);


}
