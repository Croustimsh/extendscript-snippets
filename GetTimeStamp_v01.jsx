﻿function getTimeStamp () {
 //-------------------------------------------------------------------------
 //-- G E T T I M E S T A M P
 //-------------------------------------------------------------------------
 //-- Generic: Yes
 //-------------------------------------------------------------------------
 //-- Purpose: To create a time stamp string in the format of:
 //-- yyyymmdd hh:mm:ss.mss
 //-- 2009/06/13 01:23:45.678
 //-------------------------------------------------------------------------
 //-- Paremeters: none
 //-------------------------------------------------------------------------
 //-- Calls:
 //--  PadWithZeros: Internal function to pad with zeros
 //-------------------------------------------------------------------------
 //-- Sample call:
 //--  var ts = getTimeStamp() ;
 //-------------------------------------------------------------------------
 //-- Returns: A string of the current time as yyyy.mm.dd hh:mm:ss.mss
 //-------------------------------------------------------------------------
 //-- Written 2009.09.20 by Jon S. Winters
 //-- eps@electronicpublishingsupport.com
 //-------------------------------------------------------------------------
 
 //-- Use the Date object to grab the time right now
 var now = new Date() ;
 //-- Construct the full string
 return (now.getFullYear() ) +
  PadWithZeros( now.getMonth() + 1 , 2 ) +
  PadWithZeros( now.getDate() , 2 ) + "_" +
  PadWithZeros( now.getHours() , 2 ) + 
  PadWithZeros( now.getMinutes() , 2 );
  //+ "_" + PadWithZeros( now.getSeconds() , 2 );
 //-- End of main body.
 
 //-- Next is internal function
 function PadWithZeros( value , digits ) {
  //---------------------------------------------------------------------
  //-- P A D W I T H Z E R O S
  //---------------------------------------------------------------------
  //-- Generic: Yes.
  //---------------------------------------------------------------------
  //-- Purpose: To pad a number with zeros.
  //---------------------------------------------------------------------
  //-- Parameters: 2
  //--  value: the number to pad
  //--  digits: How many total digits should the number become
  //---------------------------------------------------------------------
  //-- Returns: A string version of the number padded to the number of
  //--  digits specified. If the value is already that number of
  //--  digits or longer, the value is returned.
  //---------------------------------------------------------------------
  //-- Calls: Nothing
  //---------------------------------------------------------------------
  //-- Written: 2009.09.20 by Jon S. Winters
  //---------------------------------------------------------------------
  
  //-- Change the number into a string
  var paddedString = String ( value ) ;
  //-- Check the length of the string. And continue adding zeros to the
  //--  front until the desired number of digits is reached.
  while( digits > paddedString.length ) {
   paddedString = '0' + paddedString ;
  }
  return paddedString ;
 }
}

 //-- end of internal function