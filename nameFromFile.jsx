﻿// shamelessly rripped of https://indisnip.wordpress.com/2010/08/14/extract-file-name-and-extension/
/*var myFileName = myFile.fileName();
var myFileExt = myFile.fileExt();
*/

File.prototype.fileName = function(){
    return this.name.replace(/.[^.]+$/,'');
}

File.prototype.fileExt = function(){
    return this.name.replace(/^.*\./,'');
}
