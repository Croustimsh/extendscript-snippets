//#include nameFromFile.jsx; 
//#include GetTimeStamp_v01.jsx;
//Errors Messages
var err_fileNotSaved = "(•̀o•́)ง Please save this project first";
var err_customDir = "ヽ(ಠ_ಠ)ノ The Project is saved in an non 4U2C directory location, You are on your own buddy!";
//
// Variable to fill up 
var fileName;
var isUNCpath;
var isPixmap;
var projectDirName;
var renderPath = "NOT SET";
var preRenderPath = "NOT SET"; 
var projFullPath;
//
/*
var proj = app.project;
var actualComp = proj.activeItem;
*/
//var theLayer = actualComp.selectedLayer; 
//getProjectInfo();
function getProjectInfo (){
	 
	if (app.project.file){
		 projFullPath = app.project.file;

		
		isUNCpath = checkUNCPath(projFullPath);
		fileName = projFullPath.fileName();
		var masterFolderLoc = getFolderPos(projFullPath,"000_Projets_En_Cours");
		

		if (masterFolderLoc == -1){
			alert(err_customDir+" err01");
			return;
		}
			else{
			projectDirName = getFirstFolderName(projFullPath.toString().slice(masterFolderLoc+21));
			var projectsFolderLoc = getFolderPos(projFullPath,"/Projects/");
			var pixmapFolderLoc = getFolderPos(projFullPath,"/Pixmap/");
			
				if(projectsFolderLoc==-1 && pixmapFolderLoc==-1){
					//the after effect file is not in a project folder
					alert(err_customDir+" err02");
					return;
				}
				else if(projectsFolderLoc != -1){
					
					if (getFolderPos(projFullPath,"/Ae/") ==-1 && getFolderPos(projFullPath,"/AEP/") == -1 && getFolderPos(projFullPath,"/aep/") ==-1 && getFolderPos(projFullPath,"/AE/") == -1 ){
						alert(err_customDir+" err03");
						return;
					}
					else if (getFolderPos(projFullPath,"/Ae/") != -1){
					var aeFolderLoc = getFolderPos(projFullPath,"/Ae/");
					}
					else if (getFolderPos(projFullPath,"/AEP/") != -1){
					var aeFolderLoc = getFolderPos(projFullPath,"/AEP/");
					}
					else if (getFolderPos(projFullPath,"/aep/") != -1){
					var aeFolderLoc = getFolderPos(projFullPath,"/aep/");
					}
					else if (getFolderPos(projFullPath,"/AE/") != -1){
					var aeFolderLoc = getFolderPos(projFullPath,"/AE/");
					}
					renderPath = generatePath(projFullPath.toString().slice(0, aeFolderLoc),"/Renders/");
					preRenderPath = generatePath(projFullPath.toString().slice(0, aeFolderLoc),"/PreRender/");
				}
				else{ 
					isPixmap = true;
					renderPath = generatePath(projFullPath.toString().slice(0, pixmapFolderLoc),"/Pixmap/Pixmap_OUT/")
					preRenderPath = "Not relevant, its a pixmap dude"
					//alert("ޏ₍ ὸ.ό₎ރ You are rendering a Pixmap, Please set the output path manually.");
				}


			
			//CheckInfos();
			}
		}
	else{alert(err_fileNotSaved)}
}

//FUNCTIONS DECLARATION
	//checking if the path is on network or not
	function checkUNCPath (path){
		strPath = path.toString();
		
		if (strPath.slice(0, 2) == "//"){
			//$.writeln("UNC PATH DETECTED");
			return true  }

		else {
			//$.writeln("LOCAL PATH DETECTED");
			return false }			
	}

	//geting project folder postion 
	function getFolderPos(path,str){
		strPath = path.toString();
		
		return strPath.indexOf(str);
	}

	//Trimming to the first "/" to get the folder name
	function getFirstFolderName(path){
		strPath = path.toString();
		var cutIndex = strPath.indexOf("/");
		return strPath.slice(0,cutIndex);
	}
	function generatePath(path,folder){
		return path.toString()+folder;
	}

	function CheckInfos(){
		$.writeln("///////////PROJECT INFOS////////////");
		$.writeln("Time of the request: "+ getTimeStamp())
		$.writeln("Project : " +projectDirName);
		$.writeln("File name: " +fileName);
		$.writeln("Using UNC: " +isUNCpath);
		$.writeln("Render path: " +renderPath);
		$.writeln("Pre Render path: " +preRenderPath);
		$.writeln("Full path: " +projFullPath.toString());
		$.writeln("/////////////////DONE/////////////////");
		}