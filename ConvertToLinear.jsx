/*Simple adaptation of the python script found here:
https://stackoverflow.com/questions/34472375/linear-to-srgb-conversion?rq=1
fast Approximation of color Conversion from sRgb To Linear*/


function getLinear(actualColorChannel){
	var linColor ;
	if(actualColorChannel <= 0.0404482362771082){
	    linColor = actualColorChannel/12.92 ;

	}
	else{
	    linColor = pow(((actualColorChannel+0.055)/1.055), 2.4) ;                  
	}
	return linColor ;	
}

for (i=0;i<4;i++)
	{
		newColor = push( getLinear(actualColor[i]));
	}