﻿/*
    

SimpleGoat
Copyright (c) Charlie Leroy 2017 

 
*/  
    //================
    var version = "01";
    //================
 
 #include nameFromFile.jsx; 
#include GetTimeStamp_v01.jsx;

#include getprojectInfo.jsx;
#include sendToRenderQueue.jsx;
#include checkFolderExist.jsx;
   
/// variable declaration
    
 var isPreRender ;
   
    var om_imgSeq = "_JPEG";
    var om_mov = "_HAP";
    //var chkbx_preRender = true;

/// function declaration
function main(outputMod){
    app.beginUndoGroup("Send to render")
 var proj = app.project;
var selectedComps =  proj.selection;
   

    getProjectInfo();
    //CheckInfos();

    if (outputMod == om_imgSeq){
    	renderPath = renderPath+fileName;
    	preRenderPath = preRenderPath+fileName;
    }

    renderFolderPath  = new Folder(renderPath);
    preRenderFolderPath  = new Folder(preRenderPath);
    checkFolder(renderFolderPath);

    	if (isPixmap && isPreRender){
    		alert("ಠ_ಠ this is a pixmap file, there are no PreRender folder");
    		preRenderFolderPath = renderFolderPath;
    		checkFolder(preRenderFolderPath);
            
    		}
    	else{	
    		checkFolder(preRenderFolderPath);
    	}
    	if (outputMod == om_imgSeq && isPreRender == true ){

    		checkFolder(preRenderFolderPath);
    	}
    	else if (outputMod == om_imgSeq && isPreRender == false){
    		checkFolder(renderFolderPath);
    	}

     	if(selectedComps.length >=1){
    			for (i = 0; i < selectedComps.length; i++){
    				
    				if(isPreRender == true){file_name(selectedComps[i],outputMod,preRenderPath,selectedComps[i].name+"_"+getTimeStamp());}
    				else{sendCompToRender(selectedComps[i],outputMod,renderPath,selectedComps[i].name+"_"+getTimeStamp());
                    
                }

    				}

    			}
 endUndoGroup();
        
}

      //================
      //Setting up the root folder to the AE install path
   
    Folder.current = Folder.startup; 
    Folder.current = Folder("./");
    
    //alert(Folder.current.fsName);
    var icon_imgSeq = File ("./Scripts/ScriptUI Panels/(VS2017_rndr)/seq.png");
    var icon_mov = File ("./Scripts/ScriptUI Panels/(VS2017_rndr)/mov.png");
      //================
//FONCTION UI
function UI(thisObj) {
    // Vérifier si le script est lancé depuis ScriptUI Panels ou pas
    if (thisObj instanceof Panel) { // Si oui c'est un panneau
        var myPal = thisObj;
    } else {
        var myPal = new Window("palette","VS2017 Rndr",undefined, {resizeable:true}); // Si non on crée la fenètre
    }

    if (myPal != null) {
        
        // Paramètres de marges et alignements du contenu
        myPal.alignChildren = ["fill","fill"];
        myPal.margins = 5;
        myPal.spacing = 5;
        
        
        // On ajoute le contenu de la palette
        var contenu = myPal.add("group");
        // ["X","Y"]
        contenu.alignChildren = ["fill","center"];
        contenu.margins = 5;
        contenu.spacing = 5;
        var chkbx_preRender = contenu.add ("checkbox",undefined, "Pre-render");
        chkbx_preRender.onClick = function()  { isPreRender = chkbx_preRender.value;}

        var btn_renderSeq = contenu.add ("iconbutton",undefined, icon_imgSeq);
        btn_renderSeq.onClick = function()  {
        	
             
            main(om_imgSeq);
	      	}

        var btn_renderMov = contenu.add ("iconbutton",undefined, icon_mov);
        btn_renderMov.onClick = function()  {
           
        	
			main(om_mov);

			
		}

         
        // On définit le layout et on redessine la fenètre quand elle est resizée
        myPal.layout.layout(true);
        myPal.layout.resize();
        myPal.onResizing = myPal.onResize = function () {this.layout.resize();}
         
        } //if (myPal != null)
    return myPal;
    } //FONCTION UI



// On exécute la création de l'UI
var myPalette = UI(this);
// Si c'est une fenètre (pas lancé depuis ScriptUI Panels) il faut l'afficher
if (myPalette != null && myPalette instanceof Window) {
        //myPalette.center();     
        myPalette.show();
    }



	



	

