﻿//Split renders
var splitsNumber = 5;
var myQueue = app.project.renderQueue // Creates a shortcut for the Render Queue.
var scriptName = "Do the Split!";
var safeToRunScript = true;
var alreadyRenderedItems =  new Array();
var itemsToRender =  new Array();

// List all the already render items and store them 
 function RQAlreadyDone ()
        {
                for (i = 1; i <= myQueue.numItems; ++i) {
				var curItem = myQueue.item(i);
				
				if (curItem.status ==RQItemStatus.DONE) { 
                            alreadyRenderedItems.push(curItem);
                    }                 
                }
            }
        
        
RQAlreadyDone();
// list all the comp to render
 function RQToRender ()
        {
            var RQItemNumber = myQueue.numItems;
                for (i = 1; i <= RQItemNumber; ++i) {
				var curItem = myQueue.item(i);
				
				if (curItem.status ==RQItemStatus.QUEUED) { 
                            itemsToRender.push(curItem);
                            $.writeln("RQItem infos: ")
                            var  curentTimeStart = curItem.timeSpanStart ;
                            //saving ctd into a static var
                            var staticTimeDuration = curentTimeStart;
                            // getting the number of frames to render, rounded to avoided impossible numbers
                            var splitTimeDuration = Math.round( (curItem.timeSpanDuration/splitsNumber)*curItem.comp.frameRate);
                            
                             $.writeln("length of each video:");
                             $.writeln(splitTimeDuration);
                             //duplicate the comp enough time and set the length and start:
                             for(j=1;  j<= splitsNumber; j++){
                                   curentTimeDuration = splitTimeDuration *j ;
                                 var thiscomp = curItem.duplicate();

                                  thiscomp.timeSpanStart = ((curentTimeStart+splitTimeDuration)*(j-1))/curItem.comp.frameRate ;
                                                                      
                                        if(j<splitsNumber){
                                       thiscomp.timeSpanDuration = splitTimeDuration/curItem.comp.frameRate;
                                         }

                                   
                             }
                             
                    }                 
                }
            }
RQToRender();
$.writeln(alreadyRenderedItems)

